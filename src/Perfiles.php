<?php 

namespace Adminsite\Perfiles;

use Adminsite\Perfiles\Entity\Perfil;
use Adminsite\Perfiles\Entity\Categoria;

class Perfiles
{
	public function __construct(Perfil $perfiles, Categoria $categorias)
	{
		$this->perfiles   = $perfiles;
		$this->categorias = $categorias;
	}

	public function ultimos ($limit = 6, $offset = 0)
	{
		return $this->perfiles->resume($offset, $limit);
	}

	public function get ($id)
	{
		return $this->perfiles->find($id);
	}

	public function categorias ($perfiles = false)
	{
		if ($perfiles == true) {
			return $this->categorias->allWithProfiles();
		} else {
			return $this->categorias->all();
		}
	}
}