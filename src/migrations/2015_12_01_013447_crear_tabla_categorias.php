<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaCategorias extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adm_perfiles_categorias', function(Blueprint $table)
		{
			$table->increments('id');
			$table->char('nombre', 60)->unique();
			$table->char('url', 80)->unique();
			$table->integer('ref');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adm_perfiles_categorias');
	}

}
