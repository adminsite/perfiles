<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaPerfilesVideos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adm_perfiles_videos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->char('titulo', 60);
			$table->char('url', 60);
			$table->char('video_id', 25);
			$table->integer('perfil_id');
			$table->char('proveedor', 20);
			$table->char('imagen', 80);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adm_perfiles_videos');
	}

}
