<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaPerfiles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adm_perfiles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->char('nombre', 80);
			$table->longText('descripcion');
			$table->char('telefono', 50);
			$table->char('email', 50);
			$table->char('website', 50);
			$table->char('facebook', 50);
			$table->char('twitter', 50);
			$table->char('youtube', 50);
			$table->char('instagram', 50);
			$table->enum('imagen_perfil', array(0, 1));
			$table->integer('usuario_id');
			$table->integer('categoria_id');
			$table->integer('ref');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adm_perfiles');
	}

}
