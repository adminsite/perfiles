<?php 

namespace Adminsite\Perfiles\Entity;

use Adminsite\Adm\Entity\AbstractEntity;
use Adminsite\Adm\Entity\EntityInterface;
use Adminsite\Adm\Http\InputInterface;
use Adminsite\Perfiles\Storage\PerfilesRepository;
use Adminsite\Perfiles\Validation\Laravel\PerfilValidator;

class Perfil extends AbstractEntity implements EntityInterface
{
	/**
	 * @var Adminsite\Perfiles\Storage\PerfilesRepository
	 */
	protected $repository;

	/**
	 * @var Adminsite\Adm\Validation\Laravel\PerfilValidator
	 */
	protected $createValidator;

	/**
	 * @var Illuminate\Support\MessageBag
	 */
	protected $errors;

	/**
	 * Construct
	 *
	 * @param  Adminsite\Perfiles\Storage\PerfilesRepository  $perfiles
	 * @param  Adminsite\Perfiles\Validation\Laravel\PerfilValidator  $validator
	 */
	public function __construct (PerfilesRepository $perfiles, PerfilValidator $validator, InputInterface $input)
	{
		$this->repository      = $perfiles;
		$this->createValidator = $validator;
		$this->updateValidator = $validator;
		$this->input           = $input;
	}

	public function resume ($limit, $offset)
	{
		return $this->repository->resume($limit, $offset);
	}
}