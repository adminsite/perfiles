<?php 

namespace Adminsite\Perfiles\Entity;

use Adminsite\Adm\Entity\AbstractEntity;
use Adminsite\Adm\Entity\EntityInterface;
use Adminsite\Adm\Http\InputInterface;
use Adminsite\Perfiles\Storage\CategoriasRepository;
use Adminsite\Perfiles\Validation\Laravel\CategoriaValidator;

class Categoria extends AbstractEntity implements EntityInterface
{
	/**
	 * @var Adminsite\Perfiles\Storage\PerfilesRepository
	 */
	protected $repository;

	/**
	 * @var Adminsite\Adm\Validation\Laravel\CategoriaValidator
	 */
	protected $createValidator;

	/**
	 * @var Illuminate\Support\MessageBag
	 */
	protected $errors;

	/**
	 * Construct
	 *
	 * @param  Adminsite\Perfiles\Storage\CategoriasRepository  $categorias
	 * @param  Adminsite\Perfiles\Validation\Laravel\CategoriaValidator  $validator
	 */
	public function __construct (CategoriasRepository $categorias, CategoriaValidator $validator, InputInterface $input)
	{
		$this->repository      = $categorias;
		$this->createValidator = $validator;
		$this->updateValidator = $validator;
		$this->input           = $input;
	}

	public function allWithProfiles ()
	{
		return $this->repository->getModel()->with(array('perfiles'=>function($query){
			$query->resume();
		}))->orderBy('nombre', 'asc')->get();
	}
}