<?php 

$app['router']->get('perfiles', 'PerfilesController@indexAction');
$app['router']->post('perfiles', 'PerfilesController@storeAction');
$app['router']->put('perfiles/{id}', 'PerfilesController@updateAction');
$app['router']->delete('perfiles/{id}', 'PerfilesController@deleteAction');

$app['router']->get('perfiles/categorias', 'CategoriasController@indexAction');
$app['router']->post('perfiles/categorias', 'CategoriasController@storeAction');
$app['router']->put('perfiles/categorias/{id}', 'CategoriasController@updateAction');
$app['router']->delete('perfiles/categorias/{id}', 'CategoriasController@deleteAction');