<?php 

namespace Adminsite\Perfiles\Model;

use Adminsite\Adm\Laravel\Eloquent\Model;

class Video extends Model
{
	protected $table = 'adm_perfiles_videos';

	public function perfiles ()
	{
		return $this->belongsTo('Adminsite\Perfiles\Model\Perfil');
	}
}