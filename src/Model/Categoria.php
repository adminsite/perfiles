<?php 

namespace Adminsite\Perfiles\Model;

use Adminsite\Adm\Laravel\Eloquent\Model;
use Adminsite\Adm\Http\JsonApi\ResourceInterface;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Categoria extends Model implements ResourceInterface
{
	use SoftDeletingTrait;

	protected $table = 'adm_perfiles_categorias';

	public function perfiles ()
	{
		return $this->hasMany('Adminsite\Perfiles\Model\Perfil');
	}

	public function getId ()
	{
		return $this->id;
	}

	public function getType ()
	{
		return "categoria";
	}

	public function getAttributes ()
	{
		return $this->toArray();
	}

	public function getRelationships ()
	{
		return null;
	}

	public function getSelfLink ()
	{
		return null;
	}
	
	public function getRelatedLink ()
	{
		return null;
	}
}