<?php 

namespace Adminsite\Perfiles\Model;

use Adminsite\Adm\Laravel\Eloquent\Model;
use Adminsite\Adm\Http\JsonApi\ResourceInterface;

class Perfil extends Model implements ResourceInterface
{	
	protected $hidden = array("usuario_id");
	
	protected $table  = 'adm_perfiles';

	public function video ()
	{
		return $this->hasOne('Adminsite\Perfiles\Model\Video');
	}

	public function scopeResume ($query)
	{
		return $query->select('id', 'nombre', 'imagen_perfil', 'categoria_id', 'ref');
	}

	public function getImageURL ()
	{
		return '/adm/pics/v1/perfiles/'.$this->ref;
	}

	public function getId ()
	{
		return $this->id;
	}

	public function getType ()
	{
		return "perfil";
	}

	public function getAttributes ()
	{
		return $this->toArray();
	}

	public function getRelationships ()
	{
		return array(
			"imagenes" => array(
				"data" => array(
					"type" => "imagen"
				),
				"links" => array(
					"self" => "adm/pics/v1/perfiles/".$this->ref
				)
			)
		);
	}

	public function getSelfLink ()
	{
		return null;
	}
	
	public function getRelatedLink ()
	{
		return null;
	}
}