<?php 
namespace Adminsite\Perfiles;

use Illuminate\Support\ServiceProvider;
use Adminsite\Adm\Http\InputInterface;
use Adminsite\Perfiles\Model\Perfil;
use Adminsite\Perfiles\Model\Categoria;
use Adminsite\Perfiles\Storage\EloquentPerfilesRepository;
use Adminsite\Perfiles\Storage\EloquentCategoriasRepository;
use Adminsite\Perfiles\Entity\Perfil as PerfilEntity;
use Adminsite\Perfiles\Entity\Categoria as CategoriaEntity;
use Adminsite\Perfiles\Validation\Laravel\PerfilValidator;
use Adminsite\Perfiles\Validation\Laravel\CategoriaValidator;

class PerfilesServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('adminsite/perfiles');

		if (!is_dir('adm/perfiles')) {
			@mkdir('adm/perfiles');
		}
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$app = $this->app;

		$app['perfiles'] = $app->share(function($app){
			return new Perfiles;
		});

		$app->bind(
			'Adminsite\Perfiles\Storage\PerfilesRepository',
			function () {
				return new EloquentPerfilesRepository( new Perfil );
			}
		);

		/**
		 * Perfil Entity
		 *
		 * @return Adminsite\Perfiles\Entity\Perfil
		 */
		$app->bind('Adminsite\Perfiles\Entity\Perfil', function($app)
		{
			return new PerfilEntity(
				$app->make('Adminsite\Perfiles\Storage\PerfilesRepository'),
				new PerfilValidator( $app['validator'] ),
				$app->make("Adminsite\Adm\Http\InputInterface")
			);
		});

		$app->bind(
			'Adminsite\Perfiles\Storage\CategoriasRepository',
			function () {
				return new EloquentCategoriasRepository( new Categoria );
			}
		);

		$app->bind('Adminsite\Perfiles\Entity\Categoria', function($app)
		{
			return new CategoriaEntity(
				$app->make('Adminsite\Perfiles\Storage\CategoriasRepository'),
				new CategoriaValidator( $app['validator'] ),
				$app->make("Adminsite\Adm\Http\InputInterface")
			);
		});

		$app->bind('Adminsite\Perfiles\Perfiles', function($app){
			return new Perfiles(
				$app->make('Adminsite\Perfiles\Entity\Perfil'),
				$app->make('Adminsite\Perfiles\Entity\Categoria')
			);
		});

		/**
		 * Rutas del Package
		 *
		 */
		$app['router']->group([
			'prefix'    => 'adm/api/v1',
			'namespace' => 'Adminsite\Perfiles\Http\Controller'
		], function () use ($app)
		{
			require __DIR__.'/routes.php';
		});

		$app['router']->group([
			'prefix'    => 'adm/pics/v1',
			'namespace' => 'Adminsite\Perfiles\Http\Controller'
		], function () use ($app)
		{
			$app['router']->get('perfiles/{ref}', 'ImagenesController@showAction');
		});

	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
