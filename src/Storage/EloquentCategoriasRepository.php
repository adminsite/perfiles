<?php 

namespace Adminsite\Perfiles\Storage;

use Adminsite\Adm\Utils\Str;
use Adminsite\Perfiles\Model\Categoria;

class EloquentCategoriasRepository implements CategoriasRepository
{
	public function __construct (Categoria $categoria)
	{
		$this->model = $categoria;
	}


	/**
	 * Devuelve todos los registros de la tabla
	 *
	 * @return Illuminate\Database\Eloquent\Collection
	 */
	public function all ()
	{
		return $this->model->orderBy('nombre', 'asc')->all();
	}	


	/**
	 * Leer registros de la tabla con seleccion de rango
	 *
	 * @param integer  $offset
	 * @param integer  $limit
	 * @return Illuminate\Database\Eloquent\Collection
	 */
	public function read ($offset, $limit)
	{
		return $this->model->orderBy('nombre', 'asc')->take($limit)->skip($offset)->get();
	}


	/**
	 * Encontrar un registro segun ID
	 *
	 * @param integer  $id
	 * @return Adminsite\Perfiles\Model\Perfil
	 */
	public function find ($id)
	{
		return $this->model = $this->model->find($id);
	}


	/**
	 * Crear nuevo registro
	 *
	 * @param Illuminate\Http\Request  $request
	 * @return boolean
	 */
	public function create ($input)
	{
		$this->model->nombre = mb_strtolower(trim($input->nombre), 'UTF-8');

		$url = (isset($input->url)) ? $input->url : $input->nombre;
		$this->model->url    = Str::slug($url);
		$this->model->ref    = strtotime(date('Y-m-d H:i:s'));

		return $this->model->save();
	}


	/**
	 * Actualizar valores en modelo
	 *
	 * @param Illuminate\Http\Request  $request
	 * @param integer  $id
	 * @return boolean
	 */
	public function update ($input, $id)
	{
		$perfil = $this->find($id);
		$perfil->nombre = mb_strtolower(trim($input->nombre), 'UTF-8');
		
		$url = (isset($input->url)) ? $input->url : $input->nombre;
		$perfil->url    = Str::slug($url);

		return $perfil->save();
	}


	/**
	 * Elminar registro
	 *
	 * @param integer  $id
	 * @return boolean
	 */
	public function delete ($id)
	{
		return $this->model->destroy($id);
	}

	public function getModel ()
	{
		return $this->model;
	}
}