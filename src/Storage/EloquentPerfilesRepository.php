<?php 

namespace Adminsite\Perfiles\Storage;

use Adminsite\Perfiles\Model\Perfil;
use Adminsite\Perfiles\Model\Video;

class EloquentPerfilesRepository implements PerfilesRepository
{
	public function __construct (Perfil $perfil)
	{
		$this->perfil = $perfil;
	}


	/**
	 * Devuelve todos los registros de la tabla
	 *
	 * @return Illuminate\Database\Eloquent\Collection
	 */
	public function all ()
	{
		return $this->perfil->all();
	}	


	/**
	 * Leer registros de la tabla con seleccion de rango
	 *
	 * @param integer  $offset
	 * @param integer  $limit
	 * @return Illuminate\Database\Eloquent\Collection
	 */
	public function read ($offset, $limit)
	{
		return $this->perfil->with('video')->take($limit)->skip($offset)->get();
	}


	/**
	 * Leer registros de la tabla con algunos campos
	 *
	 * @param integer  $offset
	 * @param integer  $limit
	 * @return Illuminate\Database\Eloquent\Collection
	 */
	public function resume ($offset, $limit)
	{
		return $this->perfil->resume()->take($limit)->skip($offset)->get();
	}


	/**
	 * Encontrar un registro segun ID
	 *
	 * @param integer  $id
	 * @return Adminsite\Perfiles\Model\Perfil
	 */
	public function find ($id)
	{
		return $this->perfil = $this->perfil->with('video')->where('id', $id)->first();
	}


	/**
	 * Crear nuevo registro
	 *
	 * @param Illuminate\Http\Request  $request
	 * @return boolean
	 */
	public function create ($input)
	{
		$this->perfil->nombre       = $input->nombre;
		$this->perfil->categoria_id = $input->categoria_id;
		$this->perfil->descripcion  = $input->descripcion;
		$this->perfil->telefono     = $input->telefono;
		$this->perfil->email        = $input->email;
		$this->perfil->website      = $input->website;
		$this->perfil->facebook     = $input->facebook;
		$this->perfil->twitter      = $input->twitter;
		$this->perfil->youtube      = $input->youtube;
		$this->perfil->instagram    = $input->instagram;
		$this->perfil->ref          = strtotime(date('Y-m-d H:i:s'));
		
		if ($input->has('imagen')) {
			$this->perfil->imagen_perfil = 1;
		}

		$this->perfil->save();

		if (empty($input->video_id)) {
			$video = new Video;
			$video->url       = $input->video_url;
			$video->titulo    = $input->video_titulo;
			$video->video_id  = $input->video_id;
			$video->imagen    = $input->video_imagen;
			$video->proveedor = $input->video_proveedor;

			$this->perfil->video()->save($video);
		}

		return $this->perfil;
	}


	/**
	 * Actualizar valores en modelo
	 *
	 * @param Illuminate\Http\Request  $request
	 * @param integer  $id
	 * @return boolean
	 */
	public function update ($input, $id)
	{
		$perfil = $this->find($id);

		if ($perfil) {
			$perfil->nombre       = $input->nombre;
			$perfil->categoria_id = $input->categoria_id;
			$perfil->descripcion  = $input->descripcion;
			$perfil->telefono     = $input->telefono;
			$perfil->email        = $input->email;
			$perfil->website      = $input->website;
			$perfil->facebook     = $input->facebook;
			$perfil->twitter      = $input->twitter;
			$perfil->youtube      = $input->youtube;
			$perfil->instagram    = $input->instagram;
	
			if ($input->has('imagen')) {
				$this->perfil->imagen_perfil = 1;
			}

			$perfil->save();

			if (empty($input->video_id)) {
				$perfil->video->url       = $input->video_url;
				$perfil->video->titulo    = $input->video_titulo;
				$perfil->video->video_id  = $input->video_id;
				$perfil->video->imagen    = $input->video_imagen;
				$perfil->video->proveedor = $input->video_proveedor;

				$perfil->video->save();
			}

			return $perfil;
		}
	}


	/**
	 * Elminar registro
	 *
	 * @param integer  $id
	 * @return boolean
	 */
	public function delete ($id)
	{
		return $this->perfil->destroy($id);
	}

	/**
	 * Eliminar Imagen
	 */
	public function deleteImage ($id)
	{
		$perfil = $this->find($id);
		$perfil->imagen_perfil = 0;
		
		return $perfil->save();
	}

	public function getModel ()
	{
		return $this->perfil;
	}
}