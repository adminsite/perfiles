<?php 

namespace Adminsite\Perfiles\Http\Controller;

use Adminsite\Adm\Http\JsonApi\JsonApi;
use Illuminate\Routing\Controller;
use Adminsite\Adm\Http\Request;
use Adminsite\Adm\Images\ImageUploaderInterface as ImagesUpload;
use Adminsite\Adm\Files\FileSystemInterface as FileSystem;
use Illuminate\Support\Facades\Response;
use DB;

use Adminsite\Perfiles\Entity\Perfil;

class PerfilesController extends Controller
{
	protected $request; 

	public function __construct (Perfil $perfil, 
								 Request $request, 
								 ImagesUpload $images,
								 FileSystem $files,
								 Response $response)
	{
		$this->perfil   = $perfil;
		$this->request  = $request;
		$this->response = $response;
		$this->images   = $images;
		$this->files    = $files;
	}


	/**
	 * 
	 */
	public function indexAction ()
	{
		$posts = $this->perfil->read($this->request->offset, $this->request->limit);
		
		$response = JsonApi::make("perfiles")->addCollection($posts->all())->response();
		return $this->response->json($response, 201);
	}


	/**
	 * 
	 */
	public function storeAction ()
	{
		try 
		{
			$data = $this->request->post();

			//Crear nuevo perfil
			$this->perfil->create($data);
			//Modelo Creado
			$model = $this->perfil->getModel();

			if ($this->images->has('imagen')) {
				//Crear Carpeta
				$dir = $this->files->mkdir('adm/perfiles/'.$model->ref, 0777, true, true);

				if ($dir) {
					//Guardar Imagen
					$this->images->get('imagen')->save('adm/perfiles/'.$model->ref.'/perfil.jpg');
				}
			}

			$response = JsonApi::make("perfiles", $model)->response();
			return $this->response->json($response, 201);
		} 
		catch (\Exception $e) 
		{
			return $this->response->json([
				"success"    => false,
				"message"    => $e->getMessage(),
				"validation" => $this->perfil->errors()
			], 400);
		}
	}


	/**
	 * 
	 */
	public function updateAction ($id)
	{
		try 
		{
			$data = $this->request->post();
			//Actualizar valores
			$this->perfil->update($data, $id);
			//Modelo Creado
			$model = $this->perfil->getModel();

			if ($this->images->has('imagen')) {
				//Crear Carpeta
				$dir = 'adm/perfiles/'.$model->ref;
				if (!is_dir($dir)) {
					$this->files->mkdir($dir, 0777, true, true);
				}

				if ($dir) {
					//Guardar Imagen
					$this->images->get('imagen')->save('adm/perfiles/'.$model->ref.'/perfil.jpg');
				}
			}

			$response = JsonApi::make("perfiles", $model)->response();
			return $this->response->json($response, 201);
		} 
		catch (\Exception $e) 
		{
			return $this->response->json([
				"success"    => false,
				"message"    => $e->getMessage(),
				"validation" => $this->perfil->errors()
			], 400);
		}
	}


	/**
	 * 
	 */
	public function deleteAction ($id)
	{
		$deleted = $this->perfil->delete($id);

		if ($deleted) 
		{
			$model = $this->perfil->getModel();
			//Comprobar si existe imagen
			if (is_dir('adm/perfiles/'.$model->ref)) {
				if ($this->files->rmdir('adm/perfiles/'.$model->ref)) {
					$this->perfil->deleteImage($id);
				}
			}
		}
	}
}