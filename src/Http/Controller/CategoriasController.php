<?php 

namespace Adminsite\Perfiles\Http\Controller;

use Adminsite\Adm\Http\JsonApi\JsonApi;
use Illuminate\Routing\Controller;
use Adminsite\Adm\Http\Request;
use Response;

use Adminsite\Perfiles\Entity\Categoria;

class CategoriasController extends Controller
{
	protected $request; 

	public function __construct (Categoria $categoria, 
								 Request $request, 
								 Response $response)
	{
		$this->categoria = $categoria;
		$this->request   = $request;
		$this->response  = $response;
	}


	/**
	 * 
	 */
	public function indexAction ()
	{
		$posts = $this->categoria->read($this->request->offset, $this->request->limit);

		$response = JsonApi::make("categorias")->addCollection($posts->all())->response();
		return $this->response->json($response, 201);
	}


	/**
	 * 
	 */
	public function storeAction ()
	{
		try 
		{
			//Crear nueva categoria
			$this->categoria->create( $this->request->post() );
			//Modelo Creado
			$model = $this->categoria->getModel();

			$response = JsonApi::make("categorias", $model)->response();
			return $this->response->json($response, 201);
		} 
		catch (\Exception $e) 
		{
			return $this->response->json([
				"success"    => false,
				"message"    => $e->getMessage(),
				"validation" => $this->categoria->errors()
			], 400);
		}
	}


	/**
	 * 
	 */
	public function updateAction ($id)
	{
		try 
		{
			//Actualizar valores
			$this->categoria->update($this->request->post(), $id);
			//Modelo Creado
			$model = $this->categoria->getModel();

			$response = JsonApi::make("categorias", $model)->response();
			return $this->response->json($response, 201);
		} 
		catch (\Exception $e) 
		{
			return $this->response->json([
				"success"    => false,
				"message"    => $e->getMessage(),
				"validation" => $this->categoria->errors()
			], 400);
		}
	}


	/**
	 * 
	 */
	public function deleteAction ($id)
	{
		$deleted = $this->categoria->delete($id);
	}
}