<?php 

namespace Adminsite\Perfiles\Http\Controller;

use Adminsite\Adm\Images\Imagen;
use Illuminate\Routing\Controller;
use Adminsite\Adm\Http\Request;
use Illuminate\Support\Facades\Response;

class ImagenesController extends Controller
{
	public function __construct (Imagen $imagen, Request $request, Response $response)
	{
		$this->imagen   = $imagen;
		$this->request  = $request;
		$this->response = $response;
	}

	public function showAction ($id)
	{
		$data = array(
			'filter' => $this->request->size,
			'path'   => public_path()."/adm/perfiles/{$id}/perfil.jpg"
		);

		if (!is_file($data['path'])) {
			$data['path'] = 'http://fakeimg.pl/850x450/00CED1/FFF/?text=imagen+no+encontrada';
		}

		$image = $this->imagen->cache(function($image) use ($data)
		{
			$image->make($data['path']);

			switch ($data['filter'])
			{
				case 'xs':
					$image->fit(180, 120);
				break;

				case 'sm':
					$image->fit(260, 200, function ($constraint) {
						$constraint->aspectRatio();
					});
				break;

				case 'md':
					$image->resize(640, null, function ($constraint) {
						$constraint->aspectRatio();
					});
				break;

				case 'lg':
					$image->resize(800, null, function ($constraint) {
						$constraint->aspectRatio();
					});
				break;

				default:
					$image->fit(260, 200, function ($constraint) {
						$constraint->aspectRatio();
					});
				break;
			}
			
			return $image;

		}, 10, true);

		$response = $this->response->make($image->encode('jpg'));
		$response->header('Content-Type', 'image/jpg');

		return $response;
	}
}