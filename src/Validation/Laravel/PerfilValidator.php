<?php 

namespace Adminsite\Perfiles\Validation\Laravel;

use Adminsite\Adm\Laravel\Validation\Validator;
use Adminsite\Adm\Service\Validation\ValidableInterface;

class PerfilValidator extends Validator implements ValidableInterface
{
	/**
	 * Validation for creating a new User
	 *
	 * @var array
	 */
	protected $rules = [
		"nombre"       => "required",
		"email"        => "email",
		"website"      => "url",
		"facebook"     => "url",
		"twitter"      => "url",
		"youtube"      => "url",
		"instagram"    => "url",
		"categoria_id" => "required|integer",
		"usuario_id"   => "integer"
	];
}