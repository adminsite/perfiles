<?php 

namespace Adminsite\Perfiles\Validation\Laravel;

use Adminsite\Adm\Laravel\Validation\Validator;
use Adminsite\Adm\Service\Validation\ValidableInterface;

class CategoriaValidator extends Validator implements ValidableInterface
{
	/**
	 * Validation for creating a new User
	 *
	 * @var array
	 */
	protected $rules = [
		"nombre" => "required",
		"activo" => "integer|in:0,1"
	];
}